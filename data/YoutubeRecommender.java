//Naive recommender using minimal user centric data

package com.google.api.services.samples.youtube.cmdline.data;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.services.samples.youtube.cmdline.Auth;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.*;
import com.google.common.collect.Lists;

import java.io.IOException;
import java.util.*;

/**
 * Created by nachirau on 5/2/15.
 */
public class YoutubeRecommender {

    private static YouTube youtube;
    private static int histLength = 4;  // x 5 for paginated results
    private static int numRecomm = 15;
    private static long numSimilar = 10L;
    private static  Map<String,Integer> relatedVids;
    public static void main (String[] args) {

        // This OAuth 2.0 access scope allows for read-only access to the
        // authenticated user's account, but not other types of account access.
        List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube.readonly");

        try {
            // Authorize the request.
            Credential credential = Auth.authorize(scopes, "myuploads");

            // This object is used to make YouTube Data API requests.
            youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential).setApplicationName(
                    "youtube-cmdline-myuploads-sample").build();
            YouTube.Channels.List channelRequest = youtube.channels().list("contentDetails");
            channelRequest.setMine(true);
            channelRequest.setFields("items/contentDetails,nextPageToken,pageInfo");
            ChannelListResponse channelResult = channelRequest.execute();

            List<Channel> channelsList = channelResult.getItems();

            if (channelsList != null) {

                String history = channelsList.get(0).getContentDetails().getRelatedPlaylists().getWatchHistory();
                List<PlaylistItem> playlistItemList = new ArrayList<PlaylistItem>();

                YouTube.PlaylistItems.List playlistItemRequest =
                        youtube.playlistItems().list("id,contentDetails,snippet");
                playlistItemRequest.setPlaylistId(history);

                playlistItemRequest.setFields(
                        "items(contentDetails/videoId,snippet/title,snippet/publishedAt),nextPageToken,pageInfo");

                String nextToken = "";
                int count=0;
                do {
                    playlistItemRequest.setPageToken(nextToken);
                    PlaylistItemListResponse playlistItemResult = playlistItemRequest.execute();

                    playlistItemList.addAll(playlistItemResult.getItems());
                    System.out.println("Getting history page :"+count);

                    nextToken = playlistItemResult.getNextPageToken();
                    count++;
                } while (nextToken != null && count<histLength);

                List<String> mapKeys = new ArrayList<String>();
                Map<String,String> videoIDs = new HashMap<String, String>();

                Iterator<PlaylistItem> iterator = playlistItemList.iterator();
                while(iterator.hasNext()) {
                    PlaylistItem item = iterator.next();
                    mapKeys.add(item.getContentDetails().getVideoId());

                  //  videoIDs.put(item.getContentDetails().getVideoId(),item.getSnippet().getTitle());
                }

                //fetch related vids
                YouTube.Search.List search = youtube.search().list("id,snippet");
                String apiKey = "my_key";
                search.setKey(apiKey);
                search.setType("video");

                // Map<String,Set<String>> relatedVids = new HashMap<String, Set<String>>();

                relatedVids = new HashMap<String, Integer>();

                for(int i =0;i<mapKeys.size();i++) {
                    search.setRelatedToVideoId(mapKeys.get(i));
                    search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
                    search.setMaxResults(numSimilar);
                    SearchListResponse searchResponse = search.execute();
                    List<SearchResult> searchResultList = searchResponse.getItems();
                    for(int j=0;j<searchResultList.size();j++) {
                        SearchResult result = searchResultList.get(j);
                        if(relatedVids.containsKey(result.getId().getVideoId())) {
                            int countVal = relatedVids.get(result.getId().getVideoId());
                            countVal++;
                            relatedVids.put(result.getId().getVideoId(),countVal);
                        }
                        else {
                            relatedVids.put(result.getId().getVideoId(),1);
                            videoIDs.put(result.getId().getVideoId(),result.getSnippet().getTitle());
                        }
                    }
                }

                //Get top results
                Map<String,Integer> bestMap = bestRelated();
                Set<String> bestStrings = bestMap.keySet();
                Iterator<String> bestIterator = bestStrings.iterator();
                System.out.println("\n\nTop " + numRecomm +" Results\n");
                while(bestIterator.hasNext()) {
                    String tempKey = bestIterator.next();
                //    System.out.println(tempKey + " ### " + bestMap.get(tempKey));
                    if(videoIDs.containsKey(tempKey)) {
                        System.out.println(videoIDs.get(tempKey));
                    }
                }
            }
        } catch (IOException e) {}
    }


    private static Map<String,Integer> bestRelated() {
        Map<String,Integer> bestMap = new HashMap<String, Integer>();

        int count =0 ;
        while(count<numRecomm) {
            Set<String> resultKeys = relatedVids.keySet();
            Iterator<String> iteratorResult = resultKeys.iterator();
            int max =0;
            String maxIndex="";
            while (iteratorResult.hasNext()) {
                String tempKey = iteratorResult.next();
            //    System.out.println(tempKey + " -- " + relatedVids.get(tempKey));
                if(relatedVids.get(tempKey) > max && !bestMap.containsKey(tempKey)){
                    max = relatedVids.get(tempKey);
                    maxIndex = tempKey;
                }

            }
            bestMap.put(maxIndex,max);
            count++;
        }
        return bestMap;
    }
}
