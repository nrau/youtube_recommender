//Understand the complexity of YouTube recommender factoring only watch history

package com.google.api.services.samples.youtube.cmdline.data;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.services.samples.youtube.cmdline.Auth;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.*;
import com.google.common.collect.Lists;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by nachirau on 5/2/15.
 */
public class WatchHistory {

    private static final String PROPERTIES_FILENAME = "youtube.properties";
    private static int size=0;

    /**
     * Define a global instance of a Youtube object, which will be used
     * to make YouTube Data API requests.
     */
    private static YouTube youtube;
    private static Map<String,Double> score;
    private static int topSize = 2;
    private static int histLength = 4;
    private static Map<String,Double> relatedMap;


    public static void main (String[] args) {

        // This OAuth 2.0 access scope allows for read-only access to the
        // authenticated user's account, but not other types of account access.
        List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube.readonly");

        try {
            // Authorize the request.
            Credential credential = Auth.authorize(scopes, "myuploads");

            // This object is used to make YouTube Data API requests.
            youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, credential).setApplicationName(
                    "youtube-cmdline-myuploads-sample").build();

            YouTube.Channels.List channelRequest = youtube.channels().list("contentDetails");
            channelRequest.setMine(true);
            channelRequest.setFields("items/contentDetails,nextPageToken,pageInfo");
            ChannelListResponse channelResult = channelRequest.execute();

            List<Channel> channelsList = channelResult.getItems();

            if (channelsList != null) {

                //watch history
                String history = channelsList.get(0).getContentDetails().getRelatedPlaylists().getWatchHistory();

                //Like history
                String likes = channelsList.get(0).getContentDetails().getRelatedPlaylists().getLikes();
                System.out.println("HISTORY");

                // Define a list to store items in the list of uploaded videos.
                List<PlaylistItem> playlistItemList = new ArrayList<PlaylistItem>();

                // Retrieve the playlist of the channel's uploaded videos.
                YouTube.PlaylistItems.List playlistItemRequest =
                        youtube.playlistItems().list("id,contentDetails,snippet");
                playlistItemRequest.setPlaylistId(history);

                playlistItemRequest.setFields(
                        "items(contentDetails/videoId,snippet/title,snippet/publishedAt),nextPageToken,pageInfo");

                String nextToken = "";

                int count=0;
                do {
                    playlistItemRequest.setPageToken(nextToken);
                    PlaylistItemListResponse playlistItemResult = playlistItemRequest.execute();

                    playlistItemList.addAll(playlistItemResult.getItems());
                    System.out.println("Getting history page :"+count);

                    nextToken = playlistItemResult.getNextPageToken();
                    count++;
                } while (nextToken != null && count<histLength);


                Iterator<PlaylistItem> playlistItemIterator = playlistItemList.iterator();
                score = new HashMap<String,Double>();

                while(playlistItemIterator.hasNext()) {
                    PlaylistItem item = playlistItemIterator.next();


                    YouTube.Videos.List list = youtube.videos().list("statistics");
                    list.setId(item.getContentDetails().getVideoId());
                    list.setMaxResults(10L);
                    Video v = list.execute().getItems().get(0);
                    //  System.out.println(" view count: "+v.getStatistics().getViewCount());
                    //  System.out.println(" like count: " + v.getStatistics().getLikeCount());

                    double percentage = v.getStatistics().getLikeCount().doubleValue()/v.getStatistics().getViewCount().doubleValue();
                    score.put(item.getContentDetails().getVideoId(),percentage);
                  //  score.put(item.getContentDetails().getVideoId(),v.getStatistics().getViewCount().doubleValue());
                }


                Set<String> keyset = score.keySet();
                Iterator<String> keyIterator = keyset.iterator();
               /* while(keyIterator.hasNext()) {
                    String key = keyIterator.next();
                    System.out.println("Key : " + key);
                    System.out.println("Value : " + score.get(key));
                }*/

                String[] top = new String[topSize];
                for(int k =0;k<topSize;k++)
                top[k]=getMapHighest(keyset);


                for(int i=0;i<topSize;i++) {
                    System.out.println(top[i]);
                }

                //Get relatedvideos
                YouTube.Search.List search = youtube.search().list("id,snippet");
                String apiKey = "my_key";
                search.setKey(apiKey);
                search.setType("video");


                relatedMap = new HashMap<String,Double>();

                for(int i=0;i<topSize;i++) {

                    search.setRelatedToVideoId(top[i]);
                    search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
                    search.setMaxResults(5L);
                    SearchListResponse searchResponse = search.execute();
                    List<SearchResult> searchResultList = searchResponse.getItems();


                    System.out.println("\nSIMILAR");
                    if (searchResultList != null) {
                        Iterator<SearchResult> iterator = searchResultList.iterator();
                        while (iterator.hasNext()) {
                            SearchResult result = iterator.next();
                            System.out.print("\n" + (i + 1) + ". ID :" + result.getId().getVideoId());
                            System.out.print(" ::: Title :" + result.getSnippet().getTitle());
                            double[] viewcount = getViewCount(result);
                            relatedMap.put(result.getId().getVideoId(), Double.valueOf(viewcount[1] / viewcount[0]));
                           // relatedMap.put(result.getId().getVideoId(), viewcount[1]);
                        }
                        Set<String> relatedMapKeys = relatedMap.keySet();

                      /*  Iterator<String> iteratorTest = relatedMapKeys.iterator();
                        while(iteratorTest.hasNext()) {
                      //      System.out.println("Value is " + relatedMap.get(iteratorTest.next()));
                        }*/

                        List<String> relatedSorted = sortRelated();

                        System.out.println("\n\nORIGINAL RELATED CONTENT to :" + top[i] + "\n");
                        Iterator<String> iterator1 = relatedMapKeys.iterator();
                        while(iterator1.hasNext()) {
                            System.out.println(iterator1.next());
                        }

                        System.out.println("\n\nModified RELATED CONTENT to :" + top[i] + "\n" );
                        iterator1 = relatedSorted.iterator();
                        while(iterator1.hasNext()) {
                            System.out.println(iterator1.next());
                        }



                        relatedMap.clear();
                        relatedSorted.clear();
                    }
                }
            }

        } catch (GoogleJsonResponseException e) {
            e.printStackTrace();
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }


    private static String findMaxRelated(Set<String> relatedKeys) {
        Iterator<String> iterator = relatedKeys.iterator();
        double max = 0d;
        String maxKey="";
        boolean swapped = false;
        while(iterator.hasNext()) {
            String key = iterator.next();
            if(relatedMap.get(key)>max) {
                max = relatedMap.get(key);
                maxKey=key;
                swapped=true;
            }
        }
        if(!swapped) {
            iterator = relatedKeys.iterator();
            maxKey=iterator.next();
        }
        relatedMap.put(maxKey,0d);

        return maxKey;
    }

    private static List<String> sortRelated() {
        List<String> top = new ArrayList<String>();
        Set<String> relatedKeys = relatedMap.keySet();

        System.out.println("KEYSET!!! " + relatedKeys.toString()+"\n\n");
        Iterator<String> keys = relatedKeys.iterator();
        while(keys.hasNext()) {
            String tempKey = keys.next();
            System.out.println(relatedMap.get(tempKey));
        }

        for(int i=0;i<relatedKeys.size();i++) {
            String max = findMaxRelated(relatedKeys);
            top.add(max);
           // System.out.println("\nAdding top " + max);
        }
        return top;
    }

    private static double[] getViewCount(SearchResult result) throws IOException{
        double[] viewCount = new double[2];
        YouTube.Videos.List list = youtube.videos().list("statistics");
        list.setId(result.getId().getVideoId());
        Video v = list.execute().getItems().get(0);
        viewCount[0]=v.getStatistics().getViewCount().doubleValue();
        System.out.println("VIEW COUNT :" +viewCount[0]);
        viewCount[1]=v.getStatistics().getLikeCount().doubleValue();
        System.out.println("LIKE COUNT :" +viewCount[1]);
        return viewCount;
    }

    private static void prettyPrint (int size, Iterator<PlaylistItem> playlistEntries) throws IOException {
        System.out.println("=============================================================");
        System.out.println("\t\tTotal Videos Count: " + size);
        System.out.println("=============================================================\n");
        //   FileWriter writer = new FileWriter("/Users/nachirau/Desktop/test.csv");
        //   writer.write("Title,ID,Views,Likes\n");

        while (playlistEntries.hasNext()) {
            PlaylistItem playlistItem = playlistEntries.next();
            if(playlistItem.getSnippet().getTitle().equals("Private video")
                    || playlistItem.getSnippet().getTitle().equals("Deleted video")) continue;
            System.out.println(" video name  = " + playlistItem.getSnippet().getTitle());
            System.out.println(" video id    = " + playlistItem.getContentDetails().getVideoId());
            YouTube.Videos.List list = youtube.videos().list("statistics");
            list.setId(playlistItem.getContentDetails().getVideoId());
            list.setKey("");
            Video v = list.execute().getItems().get(0);
            System.out.println(" view count: "+v.getStatistics().getViewCount());
            System.out.println(" like count: " + v.getStatistics().getLikeCount());
            System.out.println("\n-------------------------------------------------------------\n");
        /*    writer.write(playlistItem.getSnippet().getTitle().replace(",","") + "," +
                    playlistItem.getContentDetails().getVideoId() + "," +
                    v.getStatistics().getViewCount() + ","
                    + String.valueOf(v.getStatistics().getLikeCount())+"\n");*/
        }
        // writer.close();
    }

    private static String getMapHighest( Set<String> keyset) {
        // if(size<score.size()) return;
        //size++;
        Iterator<String> iterator = keyset.iterator();
        String maxIndex="";
        double max = 0;
        int tempCount =0 ;
        while(iterator.hasNext()) {
            String key = iterator.next();
            //if(tempCount==0) maxIndex=key;
            double temp = score.get(key);
            if(temp > max) {
                max = temp;
                maxIndex = key;
            }
            tempCount++;
        }
        if(max==0){
            iterator = keyset.iterator();
            maxIndex=iterator.next();
        }
        System.out.println("Max value is : " + max);
        System.out.println("Max index is : " + maxIndex);
        score.put(maxIndex,0d);
        return maxIndex;
    }
}